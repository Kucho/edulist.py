import pathlib
from datetime import datetime

import xlrd


class pasajero:
    nombres: str
    apellidos: str
    edad: int
    sexo: str
    nacionalidad: str
    documento: str
    origen: str
    destino: str
    comprobante: str
    monto: str

    def __init__(
        self,
        nombres,
        apellidos,
        edad,
        sexo,
        nacionalidad,
        documento,
        origen,
        destino,
        comprobante,
        monto,
    ):
        self.nombres = nombres
        self.apellidos = apellidos
        self.edad = edad
        self.sexo = sexo
        self.nacionalidad = nacionalidad
        self.documento = documento
        self.origen = origen
        self.destino = destino
        self.comprobante = comprobante
        self.monto = monto

    def __repr__(self):
        return "<Pasajero Nombre:%s Origen:%s Destino:%s>" % (
            self.nombres,
            self.origen,
            self.destino,
        )

    def __eq__(self, other):
        return self.apellidos == other.apellidos and self.nombres == other.nombres

    def __lt__(self, other):
        return self.apellidos < other.apellidos


class tripConfig:
    nave: str
    placa: str
    ruta: str
    trayecto: str
    fecha: str

    def __init__(self, nave, placa, ruta, trayecto, fecha):
        self.nave = nave
        self.placa = placa
        self.ruta = ruta
        self.trayecto = trayecto
        self.fecha = fecha


def check_file_exist(excel):
    file = pathlib.Path("./" + excel + ".xlsx")
    # Si el archivo existe
    if file.is_file():
        # Si el archivo existe hacer algo
        return True
    else:
        print(f"El archivo {excel}.xlsx no existe, vuelva a intentarlo")
        return False


# check_file_header retorna True si todo está bien, false si no. Asimismo, retorna el valor del index de la ruta.
def check_file_header(excel, rutas):
    # Inicializamos el valor index de la ruta
    index_ruta = -1
    # Abrimos el libro
    wb = xlrd.open_workbook("./" + excel + ".xlsx", on_demand=True)
    # Nos situamos en la primera hoja
    sheet = wb.sheet_by_index(0)

    # Verificamos si la ruta está configurada tiene configuración
    f_ruta = sheet.cell_value(0, 2).title()

    # Recorremos las rutas configuradas para verificar si la lista actual está incluída
    found = False
    for x in range(0, len(rutas)):
        if rutas[x].nombre == f_ruta:
            # print(rutas[x].nombre + ": Válido")
            found = True
            index_ruta = x
            break
    # Si es una lista no cofigurada, salimos. No podremos hacer nada
    if not found:
        print(f"{f_ruta}: No configurada")
        return False, index_ruta

    # Verificamos si el archivo tiene una fecha válida
    f_fecha = sheet.cell_value(0, 6)

    try:
        if f_fecha != datetime.strptime(f_fecha, "%d-%m-%Y").strftime("%d-%m-%Y"):
            raise ValueError
    except ValueError:
        print(f"Fecha {f_fecha}: Formato inválido")
        return False, index_ruta

    # Verificamos que no falta información de Trayecto
    f_trayecto = sheet.cell_value(0, 4).title()

    if not f_trayecto == "Surcada" or f_trayecto == "Bajada":
        print(f"Trayecto {f_trayecto}: Inválido")
        return False, index_ruta
    # else:
    #   print(f"Trayecto {f_trayecto}: Válido")

    # Verificamos que no falta información de Nave
    f_nave = sheet.cell_value(0, 8).upper()

    if not f_nave:
        print(f"Nave: No configurada")
        return False, index_ruta
    # else:
    #   print(f"Nave {f_nave}: Válido")

    # Verificamos que no falta información de Placa
    f_placa = sheet.cell_value(0, 10).upper()

    if not f_placa:
        print(f"Placa: No configurada")
        return False, index_ruta
    # else:
    #   print(f"Placa {f_placa}: Válido")

    wb.release_resources()
    del wb

    # Exportamos la configuración del archivo para luego imprimirlo
    config = tripConfig(f_nave, f_placa, f_ruta, f_trayecto, f_fecha)
    return True, index_ruta, config


def populate_psj_routes(excel, rutas, index_ruta):
    # Abrimos el libro
    wb = xlrd.open_workbook("./" + excel + ".xlsx", on_demand=True)
    # Nos situamos en la primera hoja
    sheet = wb.sheet_by_index(0)

    # Definimos los indexes de origen y destino de la ruta
    index_origen = 0
    index_destino = 0

    # Determinamos el orden de origen y destino según el trayecto
    if sheet.cell_value(0, 4) == "Surcada":
        index_origen = len(rutas[index_ruta].paradas) - 1
        index_destino = 0
        index_incremento = -1

    if sheet.cell_value(0, 4) == "Bajada":
        index_origen = 0
        index_destino = len(rutas[index_ruta].paradas) - 1
        index_incremento = 1

    # Almacenamos en base a la transformación el origen y destino de la ruta para luego exportarlo
    origin = rutas[index_ruta].paradas[index_origen]
    destiny = rutas[index_ruta].paradas[index_destino]

    # Donde se almacenará todos los detalles
    detalles = {}

    for r in range(index_origen, index_destino + index_incremento, index_incremento):
        # Se crea un diccionario que contienen tres listas: 1) directo, 2) suben  3) bajan
        detalles[rutas[index_ruta].paradas[r]] = [[] for i in range(3)]

    # Recorremos todas las filas del excel para sacar los datos y guardarlas en un diccionario
    for x in range(sheet.nrows):
        if x > 1:
            nombres = sheet.cell_value(x, 1)
            apellidos = sheet.cell_value(x, 2)
            edad = sheet.cell_value(x, 3)
            sexo = sheet.cell_value(x, 4)
            nacionalidad = sheet.cell_value(x, 5)
            documento = sheet.cell_value(x, 6)
            origen = sheet.cell_value(x, 7)
            destino = sheet.cell_value(x, 8)
            comprobante = sheet.cell_value(x, 9)
            monto = sheet.cell_value(x, 10)
            psj = pasajero(
                nombres,
                apellidos,
                edad,
                sexo,
                nacionalidad,
                documento,
                origen,
                destino,
                comprobante,
                monto,
            )
            # Si el origen y destino de la hoja coincide con el de la ruta, va a la lista "directo"
            if origen == origin and destino == destiny:
                detalles[origen][0].append(psj)
            # Si no es directo, guardamos el origen y destino
            detalles[origen][1].append(psj)
            detalles[destino][2].append(psj)

    # Ordenamos todos los pasajeros
    for punto in detalles:
        sorted_directo = sorted(detalles[punto][0])
        detalles[punto][0] = sorted_directo

        sorted_suben = sorted(detalles[punto][1])
        detalles[punto][1] = sorted_suben

        sorted_bajan = sorted(detalles[punto][2])
        detalles[punto][2] = sorted_bajan

    return origin, destiny, detalles

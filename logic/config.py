import csv
import pathlib
import re
from typing import NamedTuple


class ruta(NamedTuple):
    nombre: str
    paradas: list


def check_folder_exist():

    # Empezamos diciendo que no existe la carpeta "stops"
    found = False
    stops_folder = pathlib.Path("./stops")
    # Verifica si existe la carpeta con las paradas
    if not stops_folder.is_dir():
        print('Carpeta "stops" no encontrada.')
    else:
        found = True
    # Retorna True si existe la carpeta, False si no
    return found


def check_files_exist():
    # Recorre todos los archivos del Path "stops" y comprueba que sea archivo y tenga la extensión .csv. Luego, guarda la dirección en la lista
    lista = [
        p
        for p in pathlib.Path("./stops").iterdir()
        if p.is_file() and pathlib.PurePosixPath(p).suffix == ".csv"
    ]

    # Imprime toda la lista mostrando solo el nombre del archivo y transformándolo a Título
    # for x in lista:
    #    file = pathlib.Path(x).resolve().stem
    #    print("Configuración encontrada: " + file.title())

    # Retorna la lista
    return lista


def integrity_check():
    ready = False
    if check_folder_exist():
        ready = True
    return ready


def populate_routes():
    # Verifica que los archivos contengan información
    lista = check_files_exist()

    pattern = re.compile("[A-zÀ-ú0-9/-]+")
    rutas = []

    if lista:
        for x in lista:
            # Definimos la lista vacía donde se almacenarán las paradas
            p = []
            with open(
                pathlib.Path(x).absolute(), encoding="utf8", mode="r"
            ) as csv_file:
                csv_reader = csv.reader(csv_file, dialect="excel")
                line_count = 0
                r_nombre = ""
                for row in csv_reader:
                    # Leemos la data y extraemos los valores que nos sirven
                    data = " ".join(pattern.findall(" ".join(row).title()))
                    if line_count == 0:
                        # Si estamos en la cabecera, asignamos el nombre de la ruta
                        r_nombre = data
                    else:
                        # Agregamos las paradas a la lista
                        p.append(data)
                    line_count += 1
                # Agregamos la ruta a la lista de rutas
                rutas.append(ruta(r_nombre, p))
    return rutas

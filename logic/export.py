import xlsxwriter
from logic.process import tripConfig


def export_to_excel(detalle: dict, config: tripConfig, origen: str, destino: str):

    # Create a workbook and add a worksheet.
    workbook = xlsxwriter.Workbook(
        f"LISTA {config.nave} - {config.fecha} - {config.trayecto}.xlsx"
    )

    worksheet1 = workbook.add_worksheet("SUPERVISOR - RUTA")
    worksheet2 = workbook.add_worksheet("CAPITANÍA - ORIGEN")
    worksheet3 = workbook.add_worksheet("CAPITANÍA - DESTINO")

    # La data empieza desde la fila 5. Sería el index 4. Sin embargo, restamos 1 por inicio de numeración de pasajeros
    row_hoja_1 = 3
    row_hoja_2 = 3
    row_hoja_3 = 3
    col_cap = 0
    col_suben = 0
    col_bajan = 6

    # Definimos los formatos a utilizar
    formato_cuadro_punto = workbook.add_format(
        {"bold": 1, "align": "center", "valign": "vcenter", "font_size": 13}
    )
    formato_cuadro_punto.set_bg_color("silver")

    formato_cuadro_accion = workbook.add_format(
        {"bold": 1, "align": "center", "valign": "vcenter", "font_size": 12}
    )

    formato_cuadro_cabecera = workbook.add_format(
        {
            "bold": 1,
            "border": 1,
            "align": "center",
            "valign": "vcenter",
            "font_size": 11,
        }
    )

    formato_cuadro_nombres = workbook.add_format(
        {"border": 1, "align": "left", "valign": "vcenter", "font_size": 11}
    )

    formato_cuadro_cuerpo = workbook.add_format(
        {"border": 1, "align": "center", "valign": "vcenter", "font_size": 11}
    )

    # Escribimos encabezado de la lista de Capitanía - Origen
    SupHeadCap = [
        "Nº",
        "Apellidos",
        "Nombres",
        "Edad",
        "Sexo",
        "Documento",
        "Nacionalidad",
        "Origen",
        "Destino",
    ]

    AgregarCabecera(
        worksheet2, row_hoja_2, col_cap, SupHeadCap, formato_cuadro_cabecera
    )

    # Escribimos encabezado de lista de Capitanía - Destino
    AgregarCabecera(
        worksheet3, row_hoja_3, col_cap, SupHeadCap, formato_cuadro_cabecera
    )

    # Contador general de pasajeros de capitanía
    c_cap_origen = 1
    c_cap_destino = 1

    for punto in detalle:
        # Escribimos en todo el ancho el nombre del punto
        worksheet1.merge_range(
            row_hoja_1, 0, row_hoja_1, 10, punto, formato_cuadro_punto
        )
        # Aumentamos en 1 el contador de filas
        row_hoja_1 += 1

        # Definimos un contador de pasajeros que suben y bajan
        c_suben = 1
        c_bajan = 1

        # Agregamos un subtítulo con la indicación
        worksheet1.merge_range(
            row_hoja_1,
            col_suben,
            row_hoja_1,
            col_suben + 4,
            "Suben en este punto",
            formato_cuadro_accion,
        )

        # Agregamos un subtítulo con la indicación
        worksheet1.merge_range(
            row_hoja_1,
            col_bajan,
            row_hoja_1,
            col_bajan + 4,
            "Bajan en este punto",
            formato_cuadro_accion,
        )

        # Escribimos la cabecera de los que suben
        SupHeadSuben = ["Nº", "Apellidos", "Nombres", "Documento", "Destino"]

        AgregarCabecera(
            worksheet1,
            row_hoja_1 + c_suben,
            col_suben,
            SupHeadSuben,
            formato_cuadro_cabecera,
        )

        # Escribimos la cabecera de los que bajan
        SupHeadBajan = ["Nº", "Apellidos", "Nombres", "Documento", "Origen"]

        AgregarCabecera(
            worksheet1,
            row_hoja_1 + c_bajan,
            col_bajan,
            SupHeadBajan,
            formato_cuadro_cabecera,
        )

        row_hoja_1 += 1

        # Recorremos la lista de pasajeros que SUBEN en este PUNTO y los imprimos en la hoja que corresponda
        for pasajero in detalle[punto][1]:

            AgregarFilaRutaSuben(
                worksheet1,
                row_hoja_1 + c_suben,
                col_suben,
                c_suben,
                pasajero,
                formato_cuadro_nombres,
                formato_cuadro_cuerpo,
            )
            c_suben += 1

            if pasajero.origen == origen:
                AgregarFilaCapitania(
                    worksheet2,
                    row_hoja_2 + c_cap_origen,
                    col_cap,
                    c_cap_origen,
                    pasajero,
                    formato_cuadro_nombres,
                    formato_cuadro_cuerpo,
                )
                c_cap_origen += 1

        # Recorremos la lista de pasajeros que BAJAN en este PUNTO y los imprimos en la hoja que corresponda
        for pasajero in detalle[punto][2]:

            AgregarFilaRutaBajan(
                worksheet1,
                row_hoja_1 + c_bajan,
                col_bajan,
                c_bajan,
                pasajero,
                formato_cuadro_nombres,
                formato_cuadro_cuerpo,
            )
            c_bajan += 1

            # Copiamos a este pasajero que baja en el destino final a la lista de capitanía (destino)
            if pasajero.destino == destino:
                AgregarFilaCapitania(
                    worksheet3,
                    row_hoja_3 + c_cap_destino,
                    col_cap,
                    c_cap_destino,
                    pasajero,
                    formato_cuadro_nombres,
                    formato_cuadro_cuerpo,
                )
                c_cap_destino += 1

        # Calculamos la cantidad mayor de pasajeros de subida y bajada para agregar la misma cantidad de espacio en ambos lados
        if c_suben > c_bajan:
            row_hoja_1 += c_suben + 1
        else:
            row_hoja_1 += c_bajan + 1

    # Imprimimos la plantilla con título y con márgenes
    imprimirPlantilla(workbook, worksheet1, config, 1)
    imprimirPlantilla(workbook, worksheet2, config, 2)
    imprimirPlantilla(workbook, worksheet3, config, 2)

    # Se cierra el libro
    c = 1

    while True:
        try:
            workbook.close()
            break
        except PermissionError:
            nombre = (
                f"LISTA {config.nave} - {config.fecha} - {config.trayecto} ({c}).xlsx"
            )
            workbook.filename = nombre
            workbook._store_workbook()
            c += 1

    print("[Éxito]: Se guardaron las listas en el archivo " + workbook.filename)


def imprimirPlantilla(workbook: xlsxwriter.Workbook, ws, config: tripConfig, tipo: int):
    imprimirCabecera(workbook, ws, config, tipo)
    setMargin(ws, tipo)


def imprimirCabecera(workbook: xlsxwriter.Workbook, ws, config: tripConfig, tipo: int):
    # Damos formato final
    formato_titulo = workbook.add_format(
        {
            "bold": 1,
            "border": 1,
            "align": "center",
            "valign": "vcenter",
            "font_size": 14,
        }
    )

    formato_cabecera = workbook.add_format(
        {
            "bold": 1,
            "border": 1,
            "align": "center",
            "valign": "vcenter",
            "font_size": 13,
        }
    )

    ancho_cabecera = 0

    if tipo == 1:
        ancho_cabecera = 10

    if tipo == 2:
        ancho_cabecera = 8

    ws.set_paper(9)
    ws.merge_range(0, 0, 2, 1, "", formato_titulo)
    ws.merge_range(
        0, 2, 0, ancho_cabecera, "LISTA DE ZARPE - " + ws.get_name(), formato_titulo
    )
    ws.merge_range(
        1,
        2,
        1,
        ancho_cabecera,
        f"Embarcación: {config.nave} | Placa: {config.placa} | {config.ruta}",
        formato_cabecera,
    )
    ws.merge_range(
        2,
        2,
        2,
        ancho_cabecera,
        f"Trayecto: {config.trayecto} | Fecha: {config.fecha}",
        formato_cabecera,
    )


def setMargin(ws, tipo: int):
    if tipo == 1:
        ws.insert_image(
            0, 0, "./assets/logo.png", {"x_offset": 3, "y_offset": 10, "x_scale": 0.64}
        )
        ws.set_landscape()
        ws.set_margins(0.2, 0.2, 0.2, 0.2)
        ws.set_column(0, 0, 2)
        ws.set_column(1, 2, 20)
        ws.set_column(3, 5, 10)
        ws.set_column(6, 6, 2)
        ws.set_column(7, 8, 20)
        ws.set_column(9, 12, 10)

    if tipo == 2:
        ws.insert_image(
            0, 0, "./assets/logo.png", {"x_offset": 3, "y_offset": 10, "x_scale": 0.64}
        )
        ws.set_margins(0.1, 0.1, 0.2, 0.2)
        ws.set_column(0, 0, 2)
        ws.set_column(1, 2, 20)
        ws.set_column(3, 4, 4)
        ws.set_column(5, 5, 10)
        ws.set_column(6, 10, 12)


def AgregarFilaCapitania(ws, row, col, n, pasajero, formato_nombre, formato_cuerpo):
    ws.write(row, col, n, formato_cuerpo)
    ws.write(row, col + 1, pasajero.apellidos, formato_nombre)
    ws.write(row, col + 2, pasajero.nombres, formato_nombre)
    ws.write(row, col + 3, pasajero.edad, formato_cuerpo)
    ws.write(row, col + 4, pasajero.sexo, formato_cuerpo)
    ws.write(row, col + 5, pasajero.documento, formato_cuerpo)
    ws.write(row, col + 6, pasajero.nacionalidad, formato_cuerpo)
    ws.write(row, col + 7, pasajero.origen, formato_cuerpo)
    ws.write(row, col + 8, pasajero.destino, formato_cuerpo)


def AgregarFilaRutaSuben(ws, row, col, n, pasajero, formato_nombre, formato_cuerpo):
    ws.write(row, col, n, formato_cuerpo)
    ws.write(row, col + 1, pasajero.apellidos, formato_nombre)
    ws.write(row, col + 2, pasajero.nombres, formato_nombre)
    ws.write(row, col + 3, pasajero.documento, formato_cuerpo)
    ws.write(row, col + 4, pasajero.destino, formato_cuerpo)


def AgregarFilaRutaBajan(ws, row, col, n, pasajero, formato_nombre, formato_cuerpo):
    ws.write(row, col, n, formato_cuerpo)
    ws.write(row, col + 1, pasajero.apellidos, formato_nombre)
    ws.write(row, col + 2, pasajero.nombres, formato_nombre)
    ws.write(row, col + 3, pasajero.documento, formato_cuerpo)
    ws.write(row, col + 4, pasajero.origen, formato_cuerpo)


def AgregarCabecera(ws, row, col, lista: list, formato):
    for idx, item in enumerate(lista):
        ws.write(row, col + idx, item, formato)

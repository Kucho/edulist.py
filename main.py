import logic.config as config
import logic.export as export
import logic.process as process
from pyfiglet import Figlet
import keyboard
import sys


def banner():
    banner = Figlet(font="standard")
    name = Figlet(font="straight")
    autor = Figlet(font="term", justify="right-to-left")
    print(banner.renderText("EduExpress"))
    print(name.renderText("Transformator 2.0"))
    print(autor.renderText("Autor: Victor Rodriguez"))


def main():
    if config.integrity_check():
        rutas = config.populate_routes()
        listas = input("Ingrese las listas a transformar:")

        for i in listas.split():
            if process.check_file_exist(i):
                result, index_ruta, tripconf = process.check_file_header(i, rutas)
                if result:
                    origen, destino, detalles = process.populate_psj_routes(
                        i, rutas, index_ruta
                    )
                    export.export_to_excel(detalles, tripconf, origen, destino)

        print("Presione Enter para volver a transformar o Escape para salir... ")
        while True:
            try:
                if keyboard.is_pressed("enter"):
                    main()
                if keyboard.is_pressed("esc"):
                    sys.exit()
            except SystemExit:
                break


banner()
main()
